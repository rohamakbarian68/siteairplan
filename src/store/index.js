
import { createStore } from "redux";
import Domesticflight from "../assets/images/Domesticflight.png";



const initialstate = {
    imgbackground: Domesticflight,
    btnChangeColorD: false,
    btnChangeColorI: false,
    btnChangeColorT: false,
    btnChangeColorA: false,
    btnChangeColorTo: false,
    btnChangeColorH: false,
    btnChangeColorV: false
}
const Reduser = (state = initialstate, action) => {

    switch (action.type) {

        case 'Domesticflight':
            return {
                ...state,
                imgbackground: action.payload,
                btnChangeColorD: true,
                btnChangeColorI: false,
                btnChangeColorT: false,
                btnChangeColorA: false,
                btnChangeColorTo: false,
                btnChangeColorH: false,
                btnChangeColorV: false
            }
        case 'internationalflight':
            return {
                ...state,
                imgbackground: action.payload,
                btnChangeColorD: false,
                btnChangeColorI: true,
                btnChangeColorT: false,
                btnChangeColorA: false,
                btnChangeColorTo: false,
                btnChangeColorH: false,
                btnChangeColorV: false
            }
        case 'train':
            return {
                ...state,
                imgbackground: action.payload,
                btnChangeColorD: false,
                btnChangeColorI: false,
                btnChangeColorT: true,
                btnChangeColorA: false,
                btnChangeColorTo: false,
                btnChangeColorH: false,
                btnChangeColorV: false
            }
        case 'bus':
            return {
                ...state,
                imgbackground: action.payload,
                btnChangeColorD: false,
                btnChangeColorI: false,
                btnChangeColorT: false,
                btnChangeColorA: true,
                btnChangeColorTo: false,
                btnChangeColorH: false,
                btnChangeColorV: false
            }
        case 'tour':
            return {
                ...state,
                imgbackground: action.payload,
                btnChangeColorD: false,
                btnChangeColorI: false,
                btnChangeColorT: false,
                btnChangeColorA: false,
                btnChangeColorTo: true,
                btnChangeColorH: false,
                btnChangeColorV: false
            }
        case 'hotel':
            return {
                ...state,
                imgbackground: action.payload,
                btnChangeColorD: false,
                btnChangeColorI: false,
                btnChangeColorT: false,
                btnChangeColorA: false,
                btnChangeColorTo: false,
                btnChangeColorH: true,
                btnChangeColorV: false
            }
        case 'villa':
            return {
                ...state,
                imgbackground: action.payload,
                btnChangeColorD: false,
                btnChangeColorI: false,
                btnChangeColorT: false,
                btnChangeColorA: false,
                btnChangeColorTo: false,
                btnChangeColorH: false,
                btnChangeColorV: true
            }
        default:
            return state
    }
}




const store = createStore(Reduser);
export default (store);