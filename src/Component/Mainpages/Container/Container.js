import React from 'react';
import styles from "./Container.module.css";
import images from '../../Header/image';
import { ReactComponent as AirplaneIcone } from '../../../assets/icons/airplane.svg';
import { ReactComponent as Train } from '../../../assets/icons/train.svg';
import { ReactComponent as Bus } from '../../../assets/icons/bus.svg';
import { ReactComponent as Suitcase } from '../../../assets/icons/suitcase.svg';
import { ReactComponent as Hotel } from '../../../assets/icons/hotel.svg';
import { ReactComponent as House } from '../../../assets/icons/house.svg';
import Domestic from "../Menu/Domesticflight/Domesticflight";
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';

const Container = () => {
  const imgbackground = useSelector(state => state.imgbackground);
  const dispatch = useDispatch();
  const btnChangeColorD = useSelector(state => state.btnChangeColorD);
  const btnChangeColorI = useSelector(state => state.btnChangeColorI);
  const btnChangeColorT = useSelector(state => state.btnChangeColorT);
  const btnChangeColorA = useSelector(state => state.btnChangeColorA);
  const btnChangeColorTo = useSelector(state => state.btnChangeColorTo);
  const btnChangeColorH = useSelector(state => state.btnChangeColorH);
  const btnChangeColorV = useSelector(state => state.btnChangeColorV);

  const handleClick = (e) => {
    const filterImage = images.filter(data => data.title === e.target.name)
    const filteraddress = filterImage.map(data => { return (data.image) });
    const filtertitle = filterImage.map(data => { return (data.title) })
    dispatch({ type: filtertitle[0], payload: filteraddress[0] });
  }

  return (
    <>
      <div className={styles.MainContainer}>
        <div className={styles.imageContainer}>
          <img  src={imgbackground} alt={1} ></img>
        </div>
        <div className={styles.AppContainer}>

          <div className={styles.HeaderAppContainer}>
            <div>
              <AirplaneIcone />
              <button style={btnChangeColorD ? { color: "blue", borderBottom: "5px solid blue" } : { color: "black" }} onClick={handleClick} name="Domesticflight">پرواز داخلی</button>
            </div>
            <div>
              <AirplaneIcone />
              <button style={btnChangeColorI ? { color: "blue", borderBottom: "5px solid blue" } : { color: "black" }} onClick={handleClick} name="internationalflight">پرواز خارجی</button>
            </div>
            <div >
              <Train />
              <button style={btnChangeColorT ? { color: "blue", borderBottom: "5px solid blue" } : { color: "black" }} onClick={handleClick} name="train">قطار</button>
            </div>
            <div >
              <Bus />
              <button style={btnChangeColorA ? { color: "blue", borderBottom: "5px solid blue" } : { color: "black" }} onClick={handleClick} name="bus">اتوبوس</button>
            </div>
            <div >
              <Suitcase />
              <button style={btnChangeColorTo ? { color: "blue", borderBottom: "5px solid blue" } : { color: "black" }} onClick={handleClick} name="tour">تور</button>
            </div>
            <div >
              <Hotel />
              <button style={btnChangeColorH ? { color: "blue", borderBottom: "5px solid blue" } : { color: "black" }} onClick={handleClick} name="hotel">هتل</button>
            </div>
            <div >
              <House />
              <button style={btnChangeColorV ? { color: "blue", borderBottom: "5px solid blue" } : { color: "black" }} onClick={handleClick} name="villa">ویلا و اقامتگاه</button>
            </div>
          </div>


        </div>
        <Domestic />


      </div>

    </>

  );
}
export default Container;
