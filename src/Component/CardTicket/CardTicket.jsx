import styles from './cardStyle.module.css';
import arrivedestination from "../../assets/images/arrivedestination.png"
const CardTicket = (props) => {
    return (

        <div className={styles.ticket}>
            <div className={styles.flightinfo}>
                <div className={styles.flightinfoIcon}>
                    <div className={styles.icon}> <img height={70} width={55} src={props.data.logo} alt="1" />
                    </div>
                    <div className={styles.logoname}>{props.data.logoName}</div>
                    <div className={styles.esterdad}>
                        اطلاعات پرواز
                    </div>
                </div>
                <div className={styles.flightinfoSystem}>
                    <div className={styles.system}>
                        <span>سیستمی</span>
                        <span>اکونومی</span>
                        <span>{props.data.name}</span>


                    </div>
                    <div className={styles.cityTime}>
                        <span style={{ fontSize: "20px" }}> {props.data.org}   {props.data.gotime}  </span>
                        <span><img src={arrivedestination} alt="1" /></span>
                        <span style={{ fontSize: "20px" }}> {props.data.dest}   {props.data.arriveTime}  </span>
                    </div>
                    <div className={styles.law}>
                        قوانین استرداد
                    </div>
                </div>
            </div>

            <div className={styles.costinfo}>
                <div className={styles.costinfospan}>
                    <p  ><span style={{ fontWeight: "Bold", fontSize: 25, color: "blue" }}>{props.data.cost}</span> ریال</p>
                </div>
                <button >انتخاب پرواز</button>
                <p>باقی مانده</p>
            </div>
        </div>

    )
}
export default CardTicket;