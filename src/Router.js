import React from 'react';
import { Routes, Route } from 'react-router-dom';
import  Header  from './Component/Header/Header';
import App from './App';
import Domesticticket from './Component/Mainpages/Menu/Domesticflight/Domsticticket';



class Router extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <Routes>

                    <Route exact path="/" element={<App />} />
                    <Route path="domesticticket" element={<Domesticticket />} />


                </Routes>
            </div>
        )
    }
}

export default Router;